Kubernetes local cluster running atop Minikube
=============
# Purpose
This is a sample deployment pipeline using Ansible. The magic happens in the "deployer" role.

Instead of using Kubernetes Manifest File, this solution suggests the use of service definition files (myservices.yml). I am not opinionated about this, but I find it easier to customize deployments this way. In any case, I am using YAML files as required by the assignment.

This implementation allows for custom "pipelines" to be created. In my experience, infrastructure and applications should be split into different pipelines. 

#### Parameters of the deployer role (defined in the services.yml)
my_service: "{{item}}" is a mandatory parameter when running the deployment

app_version is a recommended parameter. If it's not used, the latest artifact can be fetched by querying an external repository or by using the "latest" image (not recommended at all).

num_of_replicas can be used to declare the number of replicas in the app-kube.yml

clone_image can be used to run multiple copies of the same image

pipeline can be very useful

restart_flag can be very useful

# Requirements

Minikube.

Please note that I could have deployed this in a normal all-in-one (1 node) kubernetes cluster provisioned with Vagrant.